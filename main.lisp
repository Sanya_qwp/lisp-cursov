; TODO
; По запросу на сумму и срок формировать список возможных кредитов с указанием ежемесячной выплаты и суммарной выплаты. +
; Сортировать список по сумме. +
; Определять выдать/нет (кредитная история) +
; Возможность частичного/досрочного погашения. +

; import packages
(load "C:\\Users\\qwp\\quicklisp\\setup.lisp")
(ql:quickload :cl-csv)
(ql:quickload :cl-ppcre)

; import database
(defvar credits   (cl-csv:read-csv #P".\\database\\credits.csv"))
(defvar borrowers (cl-csv:read-csv #P".\\database\\borrowers.csv"))

(defun menu()
  (write-line "") ; equal \n
  (write-line "Hello! Select:")
  (write-line "1 - to show all credits"); +
  (write-line "2 - to calc a credit"); +
  (write-line "3 - to show sorted credits by summ") ; +
  (write-line "4 - to repayment a credit") ;+
  (write-line "0 - to exit"); +
  (read)
)

(defun returnToMenu()
  (write-line "Returning to menu")
  (write-line "----------------------------------------")
  (main)
)

(defun displayDatabase (credits borrowers)
  (loop for x in (cdr credits) do
    (setq ind (parse-integer (car x)))
    (print (append x (findBorrowers ind borrowers)))
  )
)

(defun findBorrowers (index table)
  (setq borrowers_list 
    (loop for x in (cdr table)
      collect (if (= index (parse-integer (car x))) (car (cdr x)))
    )
  )
  (remove-if #'null borrowers_list)
)

(defun getAvailableCredits(requestedMoney creditTerm creditPercent)
  (setq a (* requestedMoney 0.1)) ;down payment
  (setq s (- requestedMoney a)) ;credit summ without %
  (setq d (+ 1 (/ creditTerm 1200))) ;denominator of progression
  (setq k (/ (* (expt d creditTerm) (- d 1)) (- (expt d creditTerm) 1))) ;monthly payment ratio
  (setq monthlyPayment (* k s)) ;monthly payment
  (setq creditSum (+ a (* monthlyPayment creditTerm))) ;credit summ with %
  (write "The monthly payment is: ") (write monthlyPayment) (write-line "")
  (write "The summ of credit is: ") (write creditSum) (write-line "") 
  (write-line "Do you want to take this credit? 1 - Yes; 2 - No ") (setq flag(read)) (write-line "")
  (cond 
    ((= flag 1)
      (write-line "Enter your salary [if 33% of your salary below than montly payment, you have not enough money for this credit]: ") (setq salary(read)) (write-line "")
      (cond 
        ((>= (/ salary 3) monthlyPayment)
          (write-line "Enter your name") (setq borrowerName(read)) (write-line "")
          (addCredit requestedMoney monthlyPayment creditTerm creditPercent borrowerName)
        )
        (t 
          (write-line "Not enough money for this")
          (returnToMenu)
        )
      )
    )
    (t
      (returnToMenu)
    )
  )
)

(defun addCredit(requestedMoney monthlyPayment creditTerm creditPercent borrowerName)
  (setq newCreditId (+ 1 (parse-integer(caaddr credits))))
  (setq newCreditRow 
    (list  
      (write-to-string newCreditId)
      (write-to-string requestedMoney)
      (write-to-string monthlyPayment)
      (write-to-string creditTerm) 
      (write-to-string creditPercent)
    )
  )
  (setq newBorrowerRow 
    (list
      (write-to-string newCreditId)
      (write-to-string borrowerName)
    )
  ) 
  (setq credits   (append credits (list newCreditRow)))
  (setq borrowers (append borrowers (list newBorrowerRow)))
)

(defun getCost (row) (parse-integer (cadr row)))

(defun sortBySumm(temp)
  (if (= temp 0)
    (setq sortedCredits (sort (copy-list (cdr credits)) #'< :key #'getCost))
    (setq sortedCredits (sort (copy-list (cdr credits)) #'> :key #'getCost))
  )
  (displayDatabase sortedCredits borrowers)
)

(defun findCredit ()
  (write-line "Enter credit id") (setq targetId(read))
  (loop for x in (cdr credits) do
    (if (eq (parse-integer (car x)) targetId)
      (recalculateCredit x) 
    )
  )
)

(defun recalculateCredit(creditInfo)
  (write-line "Credit information: ") (write creditInfo) (write-line "")
  (write-line "Enter your desired repayment amount")(setq repaymentAmount(read))

  (setq creditSum (parse-integer (cadr creditInfo)))
  (setq monthPayment (parse-integer (caddr creditInfo)))
  (setq creditTerm (parse-integer (cadddr creditInfo)))
  (setq creditPercent  (cddddr creditInfo))

  (setq newCreditSum (- creditSum repaymentAmount))

  (setq a (* newCreditSum 0.1)) ;down payment
  (setq s (- newCreditSum a)) ;credit summ without %
  (setq d (+ 1 (/ creditTerm 1200))) ;denominator of progression
  (setq k (/ (* (expt d creditTerm) (- d 1)) (- (expt d creditTerm) 1))) ;monthly payment ratio
  (setq newMonthlyPayment (* k s)) ;monthly payment
  (setq newCreditSum (+ a (* newMonthlyPayment creditTerm))) ;credit summ with %
  (cond
    ((> newCreditSum 0)
      (write-line "Your new credit sum: ") (write newCreditSum) (write-line "")
      (write-line "Your new monthly payment: ") (write newMonthlyPayment) (write-line "")
    )
    (t
      (write-line "Your new credit sum is 0")
      (write-line "Your new monthly payment is 0")
    )
  )
  
  
)

(defun main()
  (defvar choose (menu))
  (cond
    ((= choose 1)
      (displayDatabase credits borrowers) (makunbound 'choose)(main)
    )
    ((= choose 2)
      (write-line "Enter the requested amount of money")  (setq requestedMoney(read))
      (write-line "Enter your credit term")               (setq creditTerm(read))
      (write-line "Enter the requested %")                (setq creditPercent(read))
      (getAvailableCredits requestedMoney creditTerm creditPercent) (makunbound 'choose)(main)
    )
    ((= choose 3)
      (write-line "0 - in increasing order")
      (write-line "1 - in decreasing order")
      (sortBySumm (read)) (makunbound 'choose)(main)
    )
    ((= choose 4)
      (findCredit)
    )
    (t
      (write-line "Program has been stoped") (makunbound 'choose)
    )
  )
)

; run
(main)